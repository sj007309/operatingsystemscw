#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(){

int child_1;
int child_2;


 child_1 = fork(); // calling fork on the first child
 if(child_1 == 0){  // checks if it is that process by checking if its equal to zero
         for(int i =0; i< 11; i++){ // for loop to show the id and counter
                 printf("Child %d process: counter = %d\n", getpid(), i);
                sleep(2) ; // sleep after every call
         }


 }


 else{
         child_2 = fork(); // calling fork on the second child
         if(child_2 == 0){ // also checks if it is the current process
         for(int i =0; i< 11; i++){
                 printf("Child %d process: counter = %d\n", getpid(), i);
                 sleep(2);
         }


 }





 else{ // will call parent first as its setting up the child processe
 for(int i =0; i<11; i++){
     printf("Parent %d process: counter = %d\n", getpid(), i);
     sleep(2);

 }

 }
 }
return 0;

}
