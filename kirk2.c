#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct my_msgbuf {
        long mtype;
        char mtext[200];
};

int main(void)
{
        struct my_msgbuf buf;
        int msqidOne; // spock
        int msqidTwo;   // starfleet
        key_t key;






        if ((key = ftok("kirk.c", 'A')) == -1) { // if equal to -1, it has failed it returning the id, so return error
                perror("ftok");
                exit(1);
        }

        if ((msqidOne = msgget(key, 0644 | IPC_CREAT)) == -1) {
                perror("msgget");
                exit(1);
        }

        if ((key = ftok("kirk.c", 'B')) == -1) {
                perror("ftok");
                exit(1);
        }

        if ((msqidTwo = msgget(key, 0666 | IPC_CREAT)) == -1) {
                perror("msgget");
                exit(1);
        }

        printf("Enter lines of text: \n");

        buf.mtype = 1;

        while(fgets(buf.mtext, sizeof buf.mtext, stdin) != NULL) {
                int len = strlen(buf.mtext);

                /* ditch newline at end, if it exists */
                if (buf.mtext[len-1] == '\n') buf.mtext[len-1] = '\0';




               if(buf.mtext[0]>=65 && buf.mtext[0]<=90){ // checks if its a Uppercase letter
                if (msgsnd(msqidTwo, &buf, len+1, 0) == -1) /* +1 for '\0' */
                        perror("msgsnd");

              }

               if(buf.mtext[0]>=97 && buf.mtext[0]<=122){ // checks if its a lowercase letter
                if (msgsnd(msqidOne, &buf, len+1, 0) == -1) /* +1 for '\0' */
                        perror("msgsnd");

               }






                //While loop end 

        }

        if (msgctl(msqidOne, IPC_RMID, NULL) == -1) { // closes spock if kirk is closed
                perror("msgctl");
                exit(1);
        }

        if (msgctl(msqidTwo, IPC_RMID, NULL) == -1) { // closes starfleet if kirk is closed
                perror("msgctl");
                exit(1);
        }

        return 0;
}
